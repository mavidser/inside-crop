document.getElementById('input_submit').onclick = function(event) {
  var xhr = new XMLHttpRequest;
  var formData = new FormData();
  file = document.getElementById('input_file').files[0];
  formData.append("file", file);
  xhr.open('POST', '/api/upload');
  xhr.addEventListener("error", function(event) {
    document.getElementById('status').innerHTML = 'An Error occurred while uploading';
  });
  xhr.onreadystatechange = () => {
    if (xhr.readyState == 4 && xhr.status == 200) {
      var response = JSON.parse(xhr.responseText);
      document.getElementById("status").innerHTML = 'Images successfully cropped: <a href="' + response.data + '">Link</a>';
    } else if (xhr.readyState == 4 && xhr.status == 400) {
      var response = JSON.parse(xhr.responseText);
      document.getElementById("status").innerHTML = 'An error occurred. ' + response.data;
    } else {
      document.getElementById("status").innerHTML = 'An unknown error occurred.';
    }
  }
  xhr.send(formData);
  return false;
}
