import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import * as express from 'express';
import expressValidator = require('express-validator');
import * as morgan from 'morgan';
import 'source-map-support/register';
import { APIError } from './lib/api_error';
import logger from './lib/logger';

dotenv.config({ path: '.env.example' });

import apiRoutes from './routes/api';
import mainRoutes from './routes/main';

const app = express();

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'pug');
app.set('views', 'assets/views');

app.use('/static', express.static('assets/static'));
app.use('/image', express.static('images'));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

app.use('/', mainRoutes);
app.use('/api', apiRoutes);

app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
  if (err instanceof APIError) {
    res.status((err as APIError).status || 400).json({
      data: err.message,
      success: false,
    });
  } else {
    logger.error(err.stack);
    if (process.env.NODE_ENV === 'development') {
      res.status(500).send(err.stack);
    } else {
      res.status(500).json({
        data: 'Server Error occurred',
        success: false,
      });
    }
  }
});

export default app;
