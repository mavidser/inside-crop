import { Router } from 'express';
import * as pageController from '../controllers/main';

const router = Router();

router.get('/', pageController.index);
router.get('/images/:id', pageController.images);

export default router;
