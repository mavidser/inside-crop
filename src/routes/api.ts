import { Router } from 'express';
import * as multer from 'multer';
import * as apiController from '../controllers/api';

const router = Router();
const upload = multer({ dest: `uploads/` });

router.get('/', apiController.index);
router.post('/upload', upload.single('file'), apiController.upload);

export default router;
