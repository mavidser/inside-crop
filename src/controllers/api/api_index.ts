import { apiController } from '../../lib/promisify_controller';

export const index = apiController(async (req) => {
  return 'API';
});
