import * as fs from 'fs-extra';
import * as path from 'path';
import * as sharp from 'sharp';
import { APIError } from '../../lib/api_error';
import { apiController } from '../../lib/promisify_controller';

export const upload = apiController(async (req) => {
  const image = sharp(req.file.path);
  const filename = req.file.filename;
  const basepath = path.join('images', filename);
  const metadata = await image.metadata();
  if (metadata.width !== 1024 || metadata.height !== 1024) {
    throw new APIError('Image should be 1024 x 1024 in size.');
  }
  const format = metadata.format;
  const data = await Promise.all([
    resize(image, 755, 400),
    resize(image, 365, 400),
    resize(image, 365, 212),
    resize(image, 380, 380),
  ]);
  await fs.mkdirs(basepath);
  await fs.move(req.file.path, path.join(basepath, `0.${format}`));
  await Promise.all(data.map(async (file, index) =>
    fs.writeFile(path.join(basepath, `${index + 1}.${format}`), file),
  ));
  return path.join('/', 'images', filename);
});

async function resize(image: sharp.SharpInstance, width: number, height: number) {
  return image
    .resize(width, height)
    .crop(sharp.strategy.entropy)
    .toBuffer();
}
