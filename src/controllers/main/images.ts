import * as fs from 'fs-extra';
import * as path from 'path';
import { pageController } from '../../lib/promisify_controller';

export const images = pageController(async (req) => {
  const id = req.params.id;
  const files = await fs.readdir(`images/${id}`);
  return ['images', {files, id}];
});
