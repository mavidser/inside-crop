import { pageController } from '../../lib/promisify_controller';

export const index = pageController(async (req) => {
  return ['index', {}];
});
