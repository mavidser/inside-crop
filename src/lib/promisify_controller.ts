import { NextFunction, Request, Response } from 'express';

type APIRouteCallback = (req: Request) => Promise<{}>;
type PageRouteCallback = (req: Request) => Promise<[string, {}]>;

export const apiController = (callback: APIRouteCallback) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      const out = await callback(req);
      res.json({
        data: out,
        success: true,
      });
    } catch (err) {
      next(err);
    }
  };
};

export const pageController = (callback: PageRouteCallback) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      const [view, variables] = await callback(req);
      res.render(view, variables);
    } catch (err) {
      next(err);
    }
  };
};
